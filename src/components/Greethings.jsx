import React from 'react';
import '../index.css';

function Greetings(props) {
    return (
        <h1 className="greetings">Halo {props.nama}</h1>
    );
  }
  
  export default Greetings;
