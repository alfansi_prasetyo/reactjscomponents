import React from 'react';
import './TugasButton.css';

function TugasButton(props) {
    return (
        <button className="button">{props.button}</button>
        );
  }

export default TugasButton;
