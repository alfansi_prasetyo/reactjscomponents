import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Tugas from './components/Tugas';
import TugasForm from './components/TugasForm';
import TugasButton from './components/TugasButton';
import * as serviceWorker from './serviceWorker';



ReactDOM.render(
  <React.StrictMode>
    {
    <div>
    <Tugas name="Alfansi" />
    <TugasForm placeholder="Username" />
    <TugasForm placeholder="Password" />
    <div className="flex-container">
    <TugasButton button="Login" />
    <TugasButton button="Cancel" />
    </div>
    </div>
    }
  </React.StrictMode>

  ,
  document.getElementById('root')
);

serviceWorker.unregister();
